﻿DROP DATABASE IF EXISTS h1;
CREATE DATABASE IF NOT EXISTS h1
CHARACTER SET utf8
COLLATE utf8_spanish2_ci;

-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

-- 
-- Set default database
--
USE h1;

--
-- Definition for table entradas
--
CREATE TABLE IF NOT EXISTS entradas (
  id int(11) NOT NULL AUTO_INCREMENT,
  texto varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_spanish2_ci;

-- 
-- Dumping data for table entradas
--

-- Table h1.entradas does not contain any data (it is empty)

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;